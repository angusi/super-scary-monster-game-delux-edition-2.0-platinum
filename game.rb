#-----------------------
# Super Scary Monster Game Delux Edition 2.0 Platinum
# game.rb
# Angus Ireland 2011
#-----------------------
# This file calls required gems
# and data files, and begins a new
# instance of the game.
#-----------------------	


require 'rubygems'
require 'json'
Dir['data/classes/*.rb'].each { |file| require_relative file }

TWM_EscapeGame.new