# Super Scary Monster Game Delux Edition 2.0 Platinum
## INSTALL
### Angus Ireland 2011

Does anybody still read INSTALL files?
This program requires no installation, simply parse it with your favourite Ruby command line.
See the README for more details.

If you have any problems, ensure you have Ruby 1.9.2 - type 'ruby -v' at the command line.
Also ensure you have the JSON gem installed - type 'gem install json' at the command line.
For help installing Ruby, please see the documentation at ruby.org
Note that for Windows, this is unnecessary, simply run game.exe