#-----------------------
# Super Scary Monster Game Delux Edition 2.0 Platinum
# data/classes/main.rb
# Angus Ireland 2011
#-----------------------
# This is the main game file.
# It contains code for staring new games,
# sending text input to the input parser,
# performing actions based on the parsed input,
# loading, saving, moving and calling other objects.
# It should not be modified just to add new objects to the game.
#-----------------------

class TWM_EscapeGame

	attr_reader :helpPrinter, :rooms, :player 
	attr_writer :helpPrinter, :rooms, :player

	def initializedata(path)
		if(path == 'new' || path == 'bew') then #bew is a very common typo...
			path = 'data/rooms.json'
		else
			path = ('saves/'+ path + '.json')
		end
		if not(File.exists?(path)) then
			return false #File not found...
		else
			# Note, the next line will open, read AND close a file, all in one line.
			roomsData = JSON.parse (File.open(path, 'rb')).read # Nifty, huh?
			return roomsData
		end
	end
	
	def initialize
		system('cls')
		system('clear') #Both of these are required for cross-platform compatibility
		@helpPrinter = HelpMeOutHere.new # An instance of the help printer is made globally available
		helpPrinter.welcome
		returnValue = ''
		@rooms = @player = visible = Hash.new #Initialise so there's no errors later
		inputManager = InputParser.new #See data/classes/input.rb
		context = 'preload' # We're not in a game
		while returnValue[0] != 'exit'
		
			if(@player['position'] == 0) then # "0" means out of rooms, woo!
				context = 'endgame'
				@player['position'] = nil
				Scores.new.setScore()
			end
			
			#What's visible for interaction?
			if(context == 'ingame') then
				visible.clear
				if(!@rooms[@player['position'].to_s][@player['view']].empty?) then
					# Things in view in the room
					visible.merge!(@rooms[@player['position'].to_s][@player['view']])
				end
				if(!@player['inventory'].empty?) then
					# Thigns in the inventory
					visible.merge!(@player['inventory'])
				end
			end
			
		
			print('# ') # Command prompt cursor thingy
			command = gets
			command.chomp! # Remove \n (player presses return at end of command)
			puts()
			returnValue = inputManager.commander(command, context, visible)
			case
				when returnValue[0] == 'endgame' then
					# You win!
					@player['position'] = 0
				when returnValue[0] == 'load' then
					# Load game data
					tmpGameData = initializedata(returnValue[1])
					if(tmpGameData==false) then
						# File not loaded
						helpPrinter.loadfailure
					else
						@rooms = tmpGameData['rooms']
						@player = tmpGameData['player']
						
						tmpGameData = false #Little bit of housekeeping..
						context = 'ingame'
						system('cls')
						system('clear')
						helpPrinter.startgame
					end
				when returnValue[0] == 'save'
					# Save, obviously.
					self.save(context)
				when returnValue[0] == 'exit'
					if(returnValue[1]=='save') then
						# Save before quit? OK!
						self.save('quit')
					end
				when returnValue[0] == 'scores'
					if(returnValue[1] == 'list')
						Scores.new.getScore()
					elsif(returnValue[1] == 'clear')
						Scores.new.clearScores()
					end
				when returnValue[0] == 'turn'
					# Each section here changes player view towards specified direction
					if(returnValue[1] != 'U')
						case
							when @player['view'] == 'N'
								case
									when returnValue[1] == 'L'
										@player['view'] = 'W'
									when returnValue[1] == 'R'
										@player['view'] = 'E'
									when returnValue[1] == 'A'
										@player['view'] = 'S'
								end
							when @player['view'] == 'E'
								case
									when returnValue[1] == 'L'
										@player['view'] = 'N'
									when returnValue[1] == 'R'
										@player['view'] = 'S'
									when returnValue[1] == 'A'
										@player['view'] = 'W'
								end
							when @player['view'] == 'S'
								case
									when returnValue[1] == 'L'
										@player['view'] = 'E'
									when returnValue[1] == 'R'
										@player['view'] = 'W'
									when returnValue[1] == 'A'
										@player['view'] = 'N'
								end
							when @player['view'] == 'W'
								case
									when returnValue[1] == 'L'
										@player['view'] = 'S'
									when returnValue[1] == 'R'
										@player['view'] = 'N'
									when returnValue[1] == 'A'
										@player['view'] = 'E'
								end
						end
						helpPrinter.lookTurn(returnValue[1])
					else
						helpPrinter.lookUnknownDirection
					end
				when returnValue[0] == 'look'
					if(returnValue[1] != 'U')
						helpPrinter.lookAt(returnValue[1])
						lookDirection = ''
						case
							when @player['view'] == 'N'
								case
									when returnValue[1] == 'L'
										lookDirection = 'W'
									when returnValue[1] == 'R'
										lookDirection = 'E'
									when returnValue[1] == 'A'
										lookDirection = 'N'
								end
							when @player['view'] == 'E'
								case
									when returnValue[1] == 'L'
										lookDirection = 'N'
									when returnValue[1] == 'R'
										lookDirection = 'S'
									when returnValue[1] == 'A'
										lookDirection = 'E'
								end
							when @player['view'] == 'S'
								case
									when returnValue[1] == 'L'
										lookDirection = 'E'
									when returnValue[1] == 'R'
										lookDirection = 'W'
									when returnValue[1] == 'A'
										lookDirection = 'S'
								end
							when @player['view'] == 'W'
								case
									when returnValue[1] == 'L'
										lookDirection = 'S'
									when returnValue[1] == 'R'
										lookDirection = 'N'
									when returnValue[1] == 'A'
										lookDirection = 'W'
								end
						end
						if(!@rooms[@player['position'].to_s][lookDirection].empty?) then
							# Print a list of visible stuffs
							helpPrinter.lookAndSee
							@rooms[@player['position'].to_s][lookDirection].each_value{|values| puts(" - #{values['type']}")}
							
						else
							helpPrinter.lookNothingThere
						end

					else
						helpPrinter.lookUnknownDirection
					end
				when returnValue[0] == 'inventory'
					if(returnValue[1]==true)
						helpPrinter.inventoryCheck
						if(!@player['inventory'].empty?) then
							# Print a list of visible stuffs
							helpPrinter.inventoryList
							puts(@player['inventory'])
							@player['inventory'].each_value{|values| puts(" - #{values['type']}")}
						else
							helpPrinter.inventoryEmpty
						end
					else
						helpPrinter.inventoryNotSpecified
					end
				when returnValue[0] == 'pickup'
					self.pickup(returnValue[1], returnValue[2])
				when returnValue[0] == 'action'
					self.action(returnValue[1], returnValue[2], returnValue[3], returnValue[4], command)
				else
					if(returnValue.empty?) then
						#A command can return a value (even simply "true") if its handled elsewhere.
						#This block is only reached if no result was returned at all.
						if(context=='ingame')
							helpPrinter.unknownGameCommand
						else
							helpPrinter.unknownSystemCommand
						end
					end
			end
		end
		system('cls')
		system('clear')
	end
	
	def pickup(object, itemType)
		#We don't want to duplicate inventory items...
		if(@player['inventory'][object].nil?)
			# Bangs are used here to ensure the edit is globally destructive/constructive
			@player['inventory'][object] = @rooms[player['position'].to_s][player['view']][object]
			@rooms[player['position'].to_s][player['view']].delete(object)
			@helpPrinter.pickUpSuccess
		else
			@helpPrinter.pickUpAlreadyInInventory
		end
	end
	
	def action(actiontype, object, itemType, verb, command)
		# The command requires an action from an object.
		
		# Note that because we're passing these parameters by reference (ie with the @ symbol),
		#  there is no need to actually process the return value...
		
		if(actiontype=='inventory') then # The object only needs to modify/see inventory items
			if(@player['inventory'].include?(object)) then
				@player['inventory'][object]["properties"] = eval(itemType).new(@player['inventory'][object]["properties"]).send(verb, command)
			else
				@rooms[player['position'].to_s][player['view']][object]["properties"] = eval(itemType).new(@rooms[player['position'].to_s][player['view']][object]["properties"]).send(verb, command)
			end
		else #actiontype==player, things can get a bit more complicated (and dangerous!) when objects have to modify player data...
			returnVal = ''
			if(@player['inventory'].include?(object)) then
				sendParam = { 'object' => @player['inventory'][object]["properties"], 'player' => @player }
				returnVal = eval(itemType).new(sendParam).send(verb, command)
			else
				sendParam = { 'object' => @rooms[@player['position'].to_s][@player['view']][object]["properties"], 'player' => @player }
				returnVal = eval(itemType).new(sendParam).send(verb, command)
			end
		end
	end
	
	def save(context)
		doSave = false # Init.
		path = '' # Init.
		if(context == 'quit') then
			doSave = true
			path = File.join('saves', 'last.json') # This is a session-quit save.
		elsif(context == 'ingame')
			leaveLoop = doSave = false # Init
			while leaveLoop != true
				helpPrinter.saveprompt
				print('# ')
				path = gets()
				if(path!="cancel\n") then
					doSave = true
					path.chomp! << '.json' # Plop ".json" on the end of the path
					path = File.join('saves', path)
					if(File.exists?(path))
						helpPrinter.saveoverwriteprompt
						print('# ')
						overwriteIt = gets()
						if(overwriteIt == 'y')
							doSave = true
							leaveLoop = true
						elsif(overwriteIt == 'n')
							doSave = false
							leaveLoop = false
						else
							helpPrinter.saveprompt
						end
					else
						break
					end
				else
					leaveLoop = true
					doSave = false
				end
			end
		else
			helpPrinter.savenotingame
			return false
		end
		if(doSave == true) then
			saveData = { 'rooms' => @rooms, 'player' => @player }
			saveFile = File.new(path, 'w+')
			saveFile.write(JSON.generate(saveData)) # Turn game data into a JSON format
			saveFile.close
			return true
		end
	end
end