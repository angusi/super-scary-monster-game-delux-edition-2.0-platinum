#-----------------------
# Super Scary Monster Game Delux Edition 2.0 Platinum
# data/classes/input.rb
# Angus Ireland 2011
#-----------------------
# This file parses all input strings.
# It can be modified to change the required inputs
# for system commands, for example into a new language.
# It should not be modified just to add new objects to the game.
#-----------------------

class InputParser < Object
	def initialize
	end
	
	def commander(commandString, context=nil, args=[])
		commandString = commandString.split
		if(commandString[0]=='llamallamaduck') then #debug/cheat
			return ['endgame']
		elsif(commandString[0]=='start') then
			return self.start(commandString, context)
		elsif(commandString[0]=='exit') then
			if(context == 'ingame') then
				return(['exit','save'])
			else
				return(['exit','nosave'])
			end
		elsif(commandString[0]=='help') then
			self.help(context, commandString)
			return ['help']
		elsif(commandString[0]=='save') then
			return ['save']
		elsif(commandString[1]=='scores')
			if(commandString[0]=='high')
				return['scores', 'list']
			elsif(commandString[0]=='clear')
				return['scores', 'clear']
			end
		elsif(commandString[0]=='turn' && (commandString[1]!="off" && commandString[1]!="on"))
			if commandString[1] == "left"
				direction = 'L'
			elsif commandString[1] == "right"
				direction = 'R'
			elsif commandString[1] == "around"
				direction = 'A'
			else
				direction = 'U'
			end
			return ['turn', direction]
		elsif(commandString[0]=='look')
			if commandString[1] == "left"
				direction = 'L'
			elsif commandString[1] == "right"
				direction = 'R'
			else
				direction = 'A'
			end
			return ['look', direction]
		elsif(commandString[0]=='check')
			if(commandString[1]=='pockets' || commandString[1] == 'pocket') then
				return ['inventory', true]
			else
				return ['inventory', false]
			end
		else
			if(context == 'ingame') then
				if(args.empty?) then
					return [] #No objects in view. None. Nil. Nada. Zilch. Zip. Zero.
				end
				matchedItem = matchedVerb = ''
				#Search for applicable items:	
				args.each do |inventoryItem, inventoryItemProperties|
					commandString.each do  |commandItem|
						if(inventoryItemProperties['type'] == commandItem)
							matchedItem = inventoryItem
							break
						end
					end
				end
				if(matchedItem=='') then
					return [] #No match for object found
				end
				
				
				itemType = args[matchedItem]['type'].capitalize
				if(commandString[0] == 'pick' && commandString[1] == 'up')
					if(eval(itemType).new('').getPickUp)
						return ['pickup', matchedItem, itemType]
					else
						return [] #Item cannot be picked up
					end
				end
				verbs = eval(itemType).new('').getVerbs
				
				#Search for applicable commands:
				commandString.each do  |commandVerb|
					if(verbs.key?(commandVerb))
						matchedVerb = commandVerb
						break
					end
					if(matchedVerb!='') then
						break
					else
						return [] #No match for verb found
					end
				end
				return ['action', verbs[matchedVerb], matchedItem, itemType, matchedVerb]
			else
				#We're not in game, and a system command wasn't recognised
				return []
			end
		end				
	end
	
	def start(commandString, context=nil)
		if(commandString[1].nil?) then
			HelpMeOutHere.new.startmissingparam
			# What kind of game? New? Saved? No idea!
			return []
		elsif(commandString[1]=='new') then
			return(['load', 'new'])
		else
			return(['load', commandString[1]])
		end
	end
	
	def help(context=nil, args=[])
		if(context=='preload') then
			HelpMeOutHere.new.preloadhelp(args)
			return true
		elsif(context=='ingame') then
			HelpMeOutHere.new.ingamehelp(args)
			return true
		end
	end
end