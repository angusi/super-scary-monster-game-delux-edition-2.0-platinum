#-----------------------
# Super Scary Monster Game Deluxe Edition 2.0 Platinum
# developers/sampleobject.rb
# Angus Ireland 2011
#-----------------------
# This document should get you started on
# creating your own objects for gameplay.
#-----------------------

class Objectname < Object # Change Objectname to your object's (friendly) name.

	#These lines are required.
	attr_writer :properties
	attr_reader :properties

	def initialize(itemProperties)
		# When your object is created, it will be passed a copy of
		#  it's own properties (and potentially the player's - see below).
		@properties = itemProperties
	end
	
	def getVerbs
		return {'someverb'=>'inventory', 'otherverb'=>'inventory'}
		# Replace 'someverb' and 'otherverb' with your verbs.
		# 'inventory' means this object can only modify its
		#   own properties.
		# If you must change properties relating to the player,
		#  such as position or inventory, change this to 'player'
	end
	
	def getPickUp
		return true
		# Return true if this item can be picked up and put
		#  in the player's own inventory. Else return false.
	end
	
	def someverb(args) #Change someverb to your verb
		# This is a sample function, for the action 'someverb' which takes
		#  the command as an argument.
		# This codeblock would be reached, if this code was used in game as-is,
		#  if the user typed, for example, "someverb objectname" or similar.
		# As an example, if 'Objectname' was changed to Flashlight and 'someverb'
		#  was changed to 'test', this block would be reached by 'test flashlight'
		
		
		# If you don't need to know the command used, skip the following
		#  block of code:
		args.split.each do |arg|
			# The command can be split up and tested to meet conditions,
			#  for example here we check if the word 'on' was typed, or if
			#  the word 'off' was typed anywhere in the command.
			if(arg == 'on') then
				param = 'on'
				break
			elsif(arg == 'off') then
				param = 'off'
				break
			end
		end
		
		
		# In this space here, you should add your code for what the object does.
		# If your verb is an 'inventory' type, the object's properties are available
		#  as an array through the @properties variable.
		# If your verb is a 'player' verb, the player's properties are available
		#  as an array through the @properties['player'] variable, and the object's
		#  properties are available through @properties['object']
		
		# You can also create your own language pack type files too,
		#  a la help.rb, and use them to in your object if you wish.
		# See help.rb in the data/classes folder for suggested syntax.
		
		# At the end, you can return your changes. This isn't /strictly/ necessary,
		#  since @properties is passed by reference... but it's good practice,
		#  and future proof against changes to this prorgamming policy.
		return @properties
	end
	
	# You can add more verbs here, as above.
	
	#You can add synonyms here, in the form 'alias :realverb :synonymverb'
	alias :someverb :otherverb
	# You can add as many as you want. All verbs, even synonyms, must still be
	#  defined above in the getVerbs function.
end