#-----------------------
# Super Scary Monster Game Deluxe Edition 2.0 Platinum
# data/classes/flashlight.rb
# Angus Ireland 2011
#-----------------------
# For documentation on how this works,
# see developers/sampleobject.rb
#-----------------------

class Flashlight < Object

	attr_writer :properties
	attr_reader :properties

	def initialize(itemProperties)
		@properties = itemProperties
	end
	
	def getVerbs
		return {'switch'=>'inventory', 'turn'=>'inventory'}
	end
	
	def helpVerbs
	end
	
	def getPickUp
		return true
	end
	
	def switch(args)
		param = ''
		args.split.each do |arg|
			if(arg == 'on') then
				param = 'on'
				break
			elsif(arg == 'off') then
				param = 'off'
				break
			end
		end
		if(param == 'on' && @properties["state"] != 1) then
			@properties["state"] = 1
			HelpMeOutHere.new.flashlightToggleOn
		elsif(param == 'off' && @properties["state"] != 0)
			@properties["state"] = 0
			HelpMeOutHere.new.flashlightToggleOff
		else
			HelpMeOutHere.new.flashlightUnknownToggleState
		end
		return @properties
	end
	
	alias :turn :switch
end