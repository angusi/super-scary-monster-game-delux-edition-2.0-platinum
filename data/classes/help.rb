#-----------------------
# Super Scary Monster Game Deluxe Edition 2.0 Platinum
# data/classes/help.rb
# Angus Ireland 2011
#-----------------------
# This file contains all the language strings required
# by the game system. It can be swapped for a different
# 'language pack' by the users.
#-----------------------

class HelpMeOutHere < Object
	def initialize
	end
	
	def welcome
		puts('Welcome to Super Scary Monster Game Deluxe Edition 2.0 Platinum')
		puts('You can type \'help\' at any time, or \'exit\' to close the game.')
		puts('For the best times, please type \'high scores\'.')
		self.startmessage
	end
	
	def startmessage
		puts('Type \'start\' followed by \'new\' for a new game, \'last\' for your previous game, or a save name to load a save.')
	end
	
	def unknownSystemCommand
		puts('Unknown Command.')
		puts('Try checking \'help\'.')
	end
	
	def unknownGameCommand
		puts('Sorry, I didn\'t understand that.')
		puts('Try checking \'help\', or rephrasing your action.')
		puts('Also, make sure the thing you\'re trying to use is within reach.')
	end
	
	def startmissingparam
		puts('Not enough information to start a game.')
		self.startmessage
	end
	
	def loadfailure
		print("\n\n\n")
		puts('Hrmm... what happened there? I couldn\'t find a game file to load...')
		puts('Perhaps you want to try again...')
		self.startmessage
	end
	
	def startgame
    	puts('You find yourself in a room... a strange room. You look around and see some stuff.')
    	puts('This seems like a familiar scenario... you were playing a Flash game that started just like this the other day...')
    	puts('******')
    	puts('Your task is to escape.')
    	puts('\'help\' is available to you. Try and escape as quickly as you can.')
  	end
  	
  	def preloadhelp(args=[])
  		args.shift
  		if(args[0]=='start')
  			self.startmessage
  		elsif(args[0]=='load')
  			puts('To begin a saved game, type \'start [savename]\'.')
  			puts('For example, if you have a saved game called \'DoorknobAnkleCold\', you would start it by typing: ')
  			puts(' start DoorknobAnkleCold')
  		elsif(args[0]=='scores')
  			puts('To display high scores, type \'high scores\'.')
  			puts('To clear the scoreboard, type \'clear scores\'.')
  		elsif(args[0]=='game')
  			self.ingamehelp(args)
  		else
  			if(args[0]!=nil)
  				puts('Unrecognised Topic.')
  			end
  			puts('Please choose a topic:')
  			puts('help start - Help on beginning a new game')
  			puts('help load - Help on loading a game')
  			puts('help scores - Help on the scoring system')
  			puts('help game - Help on gameplay')
  		end
  	end
  
  	def ingamehelp(args=[])
  		args.shift
		gamemode=''
  		if(args[0]=='game') then
  			gamemode=' game'
  			args.shift #Pop 'game' from the front of the array, and carry on as normal
  		end
  		if(args[0]=='moving')
  			puts('You can move around by typing \'turn [direction]\'.')
  			puts('Directions include \'left\', \'right\' and \'around\'.')
  			puts('You can also \'go through\' doors - if they\'re open!')
  		elsif(args[0]=='looking')
  			puts('To check what\'s within reach, you can \'check pockets\' or \'look [direction]\'.')
  			puts('Directions include \'left\', \'right\' and \'ahead\'.')
  		elsif(args[0]=='items')
  			puts('Items have verbs associated with them.')
  			puts('Type a phrase with a verb in the finite imperative tense to perform an action.')
  			puts('For example, you can \'turn on flashlight\'.')
  			puts('Some objects can also be put in your inventory - \'pick up flashlight\'.')
  		elsif(args[0]=='saving')
  			puts('Progress through a game can be stored by typing \'save\'.')
  			puts('When prompted, enter a name that you\'d like to save your game as.')
  			puts('It can be loaded later by typing \'start [save name]\'.')
  		else
  			if(args[0]!=nil)
  				puts('Unrecognised Topic.')
  			end
  			puts('Please choose a topic:')
  			puts("help#{gamemode} moving - Help on moving around")
  			puts("help#{gamemode} looking - Help on looking around")
  			puts("help#{gamemode} items - Help on using items")
  			puts("help#{gamemode} saving - Help on saving games")
  		end
  	end
  	
  	def scoresList
  		puts('The following people have completed Super Scary Monster Game Deluxe Edition 2.0 Platinum:')
  	end
  	
  	def scoresItem(player, score)
  		if(score == '1')
  			times = 'time'
  		else
  			times = 'times'
  		end
  		print(player, ' has completed the game ', score, ' ', times, ".\n")
  	end
  	
  	def scoresSaved(player)
  		print('Thanks, ', player, '. The scoreboard has been updated!', "\n\n")
  	end
  	
  	def scoresCleared
  		puts('Scoreboard cleared!')
  	end
  	
  	def scoresEmpty
  		puts('No one has completed the game yet. Be the first!')
  	end
  	
  	def lookUnknownDirection
  		puts('Where are you trying to look? I\'m not sure where to turn!')
  	end
  	
  	def lookAt(direction)
  		case
  			when direction=='L'
  				direction = 'to your left'
  			when direction=='R'
  				direction = 'to your right'
  			when direction = 'A'
  				direction = 'ahead of you'
  		end
  		print('You look ',direction,".\n")
  	end
  	
  	def lookNothingThere
  		puts('There isn\'t anything there.')
  	end
  	
  	def lookAndSee
  		puts('This is what you see:')
  	end
  	
  	def lookTurn(direction)
  		case
  			when direction=='L'
  				direction = 'to your left'
  			when direction=='R'
  				direction = 'to your right'
  			when direction = 'A'
  				direction = 'around'
  		end
  		print('You turn ',direction,".\n")
  	end
  	
  	def inventoryCheck
  		puts('You fish around in your pockets.')
  	end
  	
  	def inventoryList
  		puts('This is what you find:')
  	end
  	
  	def inventoryEmpty
  		puts('There\'s nothing there.')
  	end
  	
  	def inventoryNotSpecified
  		puts('Check where? Did you mean to check pockets?')
  	end
  	
  	def pickUpSuccess
  		puts('Your pockets are now a little fuller.')
  	end
  	
  	def pickUpAlreadyInInventory
  		puts('You take the item out, inspect it, then put it back in your pocket.')
  	end
  	
  	def savenotingame
  		puts('You can\'t save right now because you\'re not in a game.')
  	end
  	
  	def saveprompt
  		puts('Please enter a file name to save your game to, or cancel to return.')
  	end
  	
  	def saveoverwriteprompt
  		puts('A file by that name already exists. Do you want to overwrite? (Y/N)')
  	end
  	
  	def endGame
  		puts('Huzzah!')
  		puts('You win, you clever sod.')
  		puts('Thank you for playing Super Scary Monster Game Deluxe Edition 2.0 Platinum.')
  	end
  	
  	def endGameGetName
  		puts('Please type your name to be entered in the score table:')
  		print('# ')
  	end
  	
  	
  	#Object Helps:
  	
  	def flashlightUnknownToggleState
  		puts('You want to do WHAT to the flashlight?')
  	end
  	
  	def flashlightToggleOn
  		puts('Lumos! The room is filled with a bright, light. Everything is now in sharp, crisp focus.')
  	end
  	
  	def flashlightToggleOff
  		puts('Eeeek! It\'s all dark in here!')
  		puts('Luckily, your trusty hands can still feel what\'s around in front of you.')
  	end
  	
  	def doorUnlock
  		puts('Alohamora! The door is unlocked.')
  	end
  	
  	def doorAlreadyUnlocked
  		puts('That door is already open!')
  	end
  	
  	def doorPassThrough
  		puts('You go through the door.')
  	end
  	
  	def doorLocked
  		puts('Unf!')
  		puts('You splatted against the door, as it\'s locked.')
  	end
end