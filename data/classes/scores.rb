#-----------------------
# Super Scary Monster Game Delux Edition 2.0 Platinum
# data/classes/scores.rb
# Angus Ireland 2011
#-----------------------

class Scores

	def getScore()
		# Return a formatted list of scores
		scoreData = JSON.parse (File.open('data/scores.json', 'rb')).read
		if(scoreData.empty?)
			HelpMeOutHere.new.scoresEmpty
		else
			helpPrinter = HelpMeOutHere.new
			helpPrinter.scoresList
			scoreData.each do |player, score|
				helpPrinter.scoresItem(player, score)
			end
		end
		return true
	end
	
	def setScore()
		# Add a score to the list, and return the score list
		helpPrinter = HelpMeOutHere.new
		helpPrinter.endGame
		helpPrinter.endGameGetName
		playerName = gets().chomp!
		playerScores = JSON.parse (File.open('data/scores.json', 'rb')).read
		if(playerScores.has_key?(playerName)) then
			playerScores[playerName] = playerScores[playerName].to_i+1
		else
			playerScores[playerName]='1'
		end
		File.open('data/scores.json', 'w') {|f| f.write(JSON.generate(playerScores)) }
		helpPrinter.scoresSaved(playerName)
		self.getScore
		return true
	end
	
	def clearScores
		File.open('data/scores.json', 'w') {|f| f.write(JSON.generate(Hash.new)) }
		HelpMeOutHere.new.scoresCleared
	end


end