#-----------------------
# Super Scary Monster Game Deluxe Edition 2.0 Platinum
# data/classes/door.rb
# Angus Ireland 2011
#-----------------------
# For documentation on how this works,
# see developers/sampleobject.rb
#-----------------------

class Door < Object

	attr_writer :properties
	attr_reader :properties

	def initialize(itemProperties)
		@properties = itemProperties
	end
	
	def getVerbs
		return {'open'=>'inventory', 'go'=>'player'}
	end
	
	def getPickUp
		return false
	end
	
	def open(args)
		if(@properties['open']!=1) then
			@properties['open'] = 1
			HelpMeOutHere.new.doorUnlock
		else
			HelpMeOutHere.new.doorAlreadyUnlocked
		end
		return @properties
	end
	
	def go(args)
		if(@properties['object']['open']==1) then
			@properties['player']['position'] = @properties['object']['destination']
			HelpMeOutHere.new.doorPassThrough
		else
			HelpMeOutHere.new.doorLocked
		end
		return @properties
	end
	
end