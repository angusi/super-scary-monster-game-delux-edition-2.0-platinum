# Super Scary Monster Game Delux Edition 2.0 Platinum
## README
### Angus Ireland 2011

**Note:** This is an old project. I don't know if it works any more, or if this code even compiles properly.


#### Sample launch syntax

Windows:
  Double Click `game.exe`
  
Other:
`$ ruby game.rb`
  
#### Requirements

Windows:

  None

Other:

*  Ruby 1.9.2
*  Ruby Gems (Included with Ruby 1.9.2)
*  JSON Gem
   
#### Developers:
A set of commented data files are available in the developers directory.
They contain the basic format of both object and room data files.

If you wish to extend the game - add new objects or level sets, please start there.