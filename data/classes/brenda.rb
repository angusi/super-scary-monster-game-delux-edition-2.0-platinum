#-----------------------
# Super Scary Monster Game Deluxe Edition 2.0 Platinum
# data/classes/brenda.rb
# Angus Ireland 2011
#-----------------------
# For documentation on how this works,
# see developers/sampleobject.rb
#-----------------------

class Brenda < Object

	attr_writer :properties
	attr_reader :properties

	def initialize(itemProperties)
		@properties = itemProperties
	end
	
	def getVerbs
		return {'watch'=>'inventory', 'talk'=>'inventory', 'ask' => 'inventory'}
	end
	
	def getPickUp
		return true
	end
	
	def interact(args)
		puts('Your friend Brenda looks back at you quizically.')
		args.split.each do |arg|
			if(arg == 'cheese') then
				puts('She likes cheese.')
				puts('She also likes cake')
			end
			if(arg == 'toast') then
        puts('She doesn\'t like toast, though. Sadface.')
      end
		end
	end
	
	alias :talk :interact
	alias :watch :interact
	alias :ask :interact
end